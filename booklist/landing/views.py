from django.shortcuts import render
from django.http import HttpResponseRedirect


def booklist(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect("/dashboard")
    else:
        return render(request, 'landing.html', locals())
